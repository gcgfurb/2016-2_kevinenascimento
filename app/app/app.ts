import 'es6-shim';
import {App, IonicApp, Platform, NavController} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {DatabaseService} from './shared/database/database.service';
import {LoginPage} from './pages/login/login';

@App({
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  config: {}, // http://ionicframework.com/docs/v2/api/config/Config/
  providers: [DatabaseService]
})

class MyApp {

  private rootPage: any;

  constructor(private platform: Platform, private db: DatabaseService) {
    this.rootPage = LoginPage;
    this.initializeApp();
  }

  private initializeApp(): void {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      this.db.init();
    });
  }
}
