import {IonicApp, Page, NavController} from 'ionic-angular';
import {DashboardPage} from './../dashboard/dashboard'
import {UserPage} from './../user/user'
import {View} from './../../models/view'

@Page({
  templateUrl: 'build/pages/menu/menu.html'
})

export class MenuPage {

  private pages: View[] = [];
  private rootPageMenu: any;
  private menu: any;

  constructor(private app: IonicApp, private nav: NavController) {
    this.loadPages();
    this.rootPageMenu = DashboardPage;
  }

  ngAfterViewInit(){
    this.menu = this.app.getComponent('leftMenu');
    //this.nav = this.app.getComponent('nav');
  }

  openPage(view) {
    this.menu.close();
    this.nav.push(view.getComponent());
  }

  loadPages(): void {
    this.pages.push(new View('Dashboard', DashboardPage));
    this.pages.push(new View('Usuários', UserPage));
  }

}
