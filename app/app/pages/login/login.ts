import {Alert, Page, Loading, NavController} from 'ionic-angular';
import {MenuPage} from './../menu/menu';
import {DatabaseService} from './../../shared/database/database.service';


@Page({
  templateUrl: 'build/pages/login/login.html'
})
export class LoginPage {

  private username: string;
  private password: string;

  constructor(private nav: NavController, private db: DatabaseService) {

  }

  private login(): void {
    //call login service and redirect
    let loading = Loading.create({
      content: "Por favor, aguarde.",
      dismissOnPageChange: true
    });
    this.nav.present(loading);

    let params = [
      {name: 'login', operator: '=', value: this.username},
      {name: 'password', operator: '=', value: this.password}
    ];

    //TODO Corrigir para passar null;
    //this.db
    //  .findAll('user', '', params, null)
    //  .then(result => {
    //    if(result.length > 0) {
    //      console.log('Logado com sucesso!');
          this.goToDashboard();
      //  } else {
      //    this.doAlert();
      //  }
      //});
  }

  private doAlert(): void  {
    let alert = Alert.create({
      title: 'Erro!',
      subTitle: 'Usuário ou senha inválida!',
      buttons: ['OK']
    });
    this.nav.present(alert);
  }

  private goToDashboard(): void {
    this.nav.setRoot(MenuPage, null, { animate: true })
  }

}
