import {IonicApp, Page, NavController, Modal} from 'ionic-angular';
import {GamePage} from './../game/game';
import {GameRegisterPage} from './../game/game-register';
import {DatabaseService} from '../../shared/database/database.service';


@Page({
  templateUrl: 'build/pages/dashboard/dashboard.html'
})
export class DashboardPage {

  private db: DatabaseService;
  private nav: NavController;
  public games: any[];

  constructor(nav: NavController, db: DatabaseService) {
    this.nav = nav;
    this.db = db;
    this.loadGames();
  }

  public openGame(game: Object) {
    this.nav.push(GamePage, {
      game: game
    });
  }

  public openGameRegister() {
    let modalGame = Modal.create(GameRegisterPage);
    this.nav.present(modalGame);

    modalGame.onDismiss((data) => {
      this.loadGames();
    });
  }

  private loadGames() {
    this.db.findAll('game')
      .then(result => {
        this.games = result;
      });
  }
}
