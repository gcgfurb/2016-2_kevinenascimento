import {Page, NavParams, ViewController, NavController} from 'ionic-angular';
import {Camera, Toast} from 'ionic-native';
import {NgZone} from 'angular2/core';
import {DatabaseService} from '../../shared/database/database.service';

@Page({
  templateUrl: 'build/pages/game/game-register.html',
  providers: [DatabaseService]
})

export class GameRegisterPage {

  private name: string;
  private base64Image: string;
  private zone: NgZone;
  private db: DatabaseService;
  private params: NavParams;
  private symbolPage: boolean;
  private viewCtrl: ViewController;
  private nav: NavController;
  private game: any;

  constructor(db: DatabaseService, params: NavParams, viewCtrl: ViewController, nav: NavController) {
    this.base64Image = "img/no-photo.png";
    this.zone = new NgZone({enableLongStackTrace: false});
    this.db = db;
    this.params = params;
    this.symbolPage = params.data.symbol;
    this.game = params.data.game;
    this.viewCtrl = viewCtrl;
    this.nav = nav;
  }

  public takePicture(): void {

    //Get from gallery
    let options = {
      quality: 80,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: false,
      encodingType: Camera.EncodingType.JPEG,
      saveToPhotoAlbum: false
    };

    //Take a picture
    //let options = {
    //  quality : 75,
    //  destinationType : Camera.DestinationType.DATA_URL,
    //  sourceType : Camera.PictureSourceType.CAMERA,
    //  allowEdit : true,
    //  encodingType: Camera.EncodingType.JPEG,
    //  targetWidth: 300,
    //  targetHeight: 300,
    //  saveToPhotoAlbum: false
    //};

    Camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.zone.run(() => this.base64Image = "data:image/jpeg;base64," + imageData);
    }, (err) => {
      console.log(err);
    });
  }

  public saveGame(): void {
    if(this.symbolPage) {
      this.saveSymbol()
        .then((symbolId) => {
          this.dismiss({
            id: symbolId,
            name: this.name,
            image: this.base64Image,
            game: this.game.id
          });

        });
      return;
    }

    let columns = [
      {name: 'name', value: this.name},
      {name: 'background', value: this.base64Image}
    ];
    this.db.insert('game', columns);
    this.dismiss();
  }

  public dismiss(params?): void  {
    this.viewCtrl.dismiss(params);
  }

  private saveSymbol() {
    let columns = [
      {name: 'name', value: this.name},
      {name: 'image', value: this.base64Image},
      {name: 'game', value: this.game.id}
    ];
    return this.db.insert('symbol', columns)
      .then((data) => {
        return data.insertId;
    });
  }


  //
  //private dataURItoBlob(dataURI, callback): void {
  //  // convert base64 to raw binary data held in a string
  //  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  //  var byteString = atob(dataURI.split(',')[1]);
  //
  //  // separate out the mime component
  //  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
  //
  //  // write the bytes of the string to an ArrayBuffer
  //  var ab = new ArrayBuffer(byteString.length);
  //  var ia = new Uint8Array(ab);
  //  for (var i = 0; i < byteString.length; i++) {
  //    ia[i] = byteString.charCodeAt(i);
  //  }
  //
  //  // write the ArrayBuffer to a blob, and you're done
  //  var bb = new BlobBuilder();
  //  bb.append(ab);
  //  return bb.getBlob(mimeString);
  //}
}
