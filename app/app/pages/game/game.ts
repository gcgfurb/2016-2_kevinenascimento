import {Page, NavController, NavParams, Modal, Toast} from 'ionic-angular';
import {ViewChild, ElementRef} from 'angular2/core';
import {GameRegisterPage} from './../game/game-register';
import {DragDirective} from './../../shared/directives/dnd/drag';
import {DragWithResetDirective} from './../../shared/directives/dnd/drag-with-reset';
import {DatabaseService} from '../../shared/database/database.service';

@Page({
  templateUrl: 'build/pages/game/game.html',
  directives: [DragDirective, DragWithResetDirective]
})

export class GamePage {

  private params: NavParams;
  private nav: NavController;
  private game: any;
  private db: DatabaseService;
  private symbols: any[];
  private currentSymbol: any;

  @ViewChild('newSymbol') newSymbol: ElementRef;

  constructor(params: NavParams, nav: NavController, db: DatabaseService) {
    this.params = params;
    this.nav = nav;
    this.game = params.data.game;
    this.db = db;
    this.currentSymbol = false;
    this.loadSymbols();
  }

  public openSymbolRegister() {
    let modalGame = Modal.create(GameRegisterPage, {symbol: true, game: this.game});
    this.nav.present(modalGame);

    modalGame.onDismiss((symbol) => {
      this.currentSymbol = symbol;
      this.loadSymbols();
      this.showMessage();
    });
  }

  public saveSymbolPosition() {
    let currentX = this.newSymbol.nativeElement.getAttribute('currentx');
    let currentY = this.newSymbol.nativeElement.getAttribute('currenty');

    let newValues = [
      {name: 'posX', value: currentX},
      {name: 'posY', value: currentY}
    ];

    let symbolId = this.currentSymbol.id;
    let gameId = this.game.id;
    let paramters = [
      {name: 'id', value: symbolId},
      {name: 'game', value: gameId}
    ];
    this.db.update('symbol', newValues, paramters)
      .then(() => {
        this.currentSymbol = false;
        this.loadSymbols();
      });

  }

  //ta;vez trocar, pois fica travado...
  private showMessage() {
    let toast = Toast.create({
      message: 'Arraste o símbolo adicionado a possição que deseja fixar!',
      duration: 3000,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    this.nav.present(toast);
  }

  private loadSymbols() {
    let whereCondition = [{name: 'game', operator: '=', value: this.game.id}];
    this.db.findAll('symbol', [], whereCondition)
      .then(result => {
        this.symbols = result;
      });
  }
}
