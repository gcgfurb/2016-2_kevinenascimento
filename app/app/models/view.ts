import {App, Platform} from 'ionic-angular';

export class View {

  title: string;
  component: Object;

  constructor(t: string, c: Object) {
    this.title = t;
    this.component = c;
  }

  getTitle(): string {
    return this.title;
  }

  getComponent(): Object {
    return this.component;
  }
}
