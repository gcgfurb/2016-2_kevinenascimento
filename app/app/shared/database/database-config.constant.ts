import {App} from 'ionic-angular';

export class DatabaseConfig {

  //public static get TABLES(): any {
  //  export const WALK = new Table();
  //}
  //
  //public static get STRUCTURE(): Database {
  //  return new Database(
  //    'dog-app',
  //     [
  //       new Table()
  //     ]
  //  );
  //}

  public static get DATABASE_CONFIG(): any {
    return {
      database: 'editor-tagarela',
      tables: [
        {
          name: 'user',
          columns: [
            {name: 'login', type: 'text', primaryKey: true},
            {name: 'password', type: 'text'}
          ]
        },
        {
          name: 'game',
          columns: [
            {name: 'id', type: 'integer', primaryKey: true},
            {name: 'name', type: 'text'},
            {name: 'background', type: 'text'},
            {name: 'user', type: 'integer'}
          ]
        },
        {
          name: 'symbol',
          columns: [
            {name: 'id', type: 'integer', primaryKey: true},
            {name: 'name', type: 'text'},
            {name: 'image', type: 'text'},
            {name: 'sound', type: 'text'},
            {name: 'game', type: 'integer'},
            {name: 'posX', type: 'integer', defaultValue: 0},
            {name: 'posY', type: 'integer', defaultValue: 0}
          ]
        }
      ]
    };
  }
}
