import {Storage, SqlStorage} from 'ionic-angular';
import {DatabaseMigrations} from './database-migrations.constant'
import {DatabaseConfig} from './database-config.constant'

export class DatabaseService {

  private storage: Storage;

  constructor() {
    this.storage = new Storage(SqlStorage, {name: 'editor-tagarela'});
  }

  init(): void {
    let query = 'CREATE TABLE IF NOT EXISTS database_config (id integer primary key, version integer)';

    this.storage
      .query(query)
      .then(() => {
        this.findById('database_config', 1)
          .then(result => {
            if(!result) {
              //Insert our unique table row with default values
              this.insert('database_config', [{name: 'id', value: 1}, {name: 'version', value: this.getLastMigrationVersion()}]);
            }
            console.log('Table database_config verified');
          });
      })
      .then(() => {
        this.createTables();
        this.runMigrations();
      });
  }

  createTables(): void {
    DatabaseConfig.DATABASE_CONFIG.tables.forEach((table) => {
      let columns = [];

      table.columns.forEach((column) => {
        let extraArguments = '';
        if (column.primaryKey) {
          extraArguments += ' primary key ';
        }

        if (column.defaultValue) {
          extraArguments += ' DEFAULT \''+ column.defaultValue + '\'';
        }

        columns.push(column.name + ' ' + column.type + ' ' + extraArguments);

        if (column.foreign) {
          columns.push('FOREIGN KEY(' +  column.name + ') REFERENCES ' + column.foreign.table +  '(' + column.foreign.column + ')');
        }
      });

      let query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
      this.execute(query)
        .then(
        (success) => {
          console.log('Table ' + table.name + ' verified');
        },
        (error) => {
          console.error('Error on create table ' + table.name);
          console.error(error.message);
        });
    });
  }

  runMigrations(): void {
    this.findById('database_config', 1)
      .then(result => {
        let version = result.version || 0;
        this.runMigration(version);
      });
  }

  runMigration(actualVersion): void {
    //lets execute migration sequencially for all versions
    DatabaseMigrations.MIGRATIONS.forEach((migration) => {
      if (actualVersion < migration.version) {
        this.executeMigration(migration.version, migration.queries, 0);
      }
    });
  }

  //execute each version migration query and only when it's finished update the database version
  executeMigration(version, queries, index): void {
    this.execute(queries[index])
      .then(
      (success) => {
        index++;
        console.log('executeMigration version: ' + version);
        if (typeof queries[index] !== 'undefined') {
          //if there is another query, let's execute it
          this.executeMigration(version, queries, index);
        } else {
          //if we haven't other queries, we finished this version, lets update our actual version
          this.updateVersion(version);
        }
      },
      (error) => {
        this.executeMigration(version, queries, index);
      }
    );
  }

  //update database version
  updateVersion(newVersion): void {
    this.update('database_config', [{name: 'version', value: parseInt(newVersion)}], [{name: 'id', value: 1}]);
  }

  getLastMigrationVersion(): number {
    let maxVersion: number = 0;
    DatabaseMigrations.MIGRATIONS.forEach((migration) => {
      if(migration.version > maxVersion) {
        maxVersion = migration.version;
      }
    });
    return maxVersion;
  }

  /**
   * Execute some query
   *
   * @param query
   * @param bindings
   * @returns {Promise}
   */
  execute(query, bindings?): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage
        .query(query, bindings)
        .then((transaction) => {
          resolve(transaction.res);
        }, (error) => {
          console.error('Transaction ERROR');
          console.error('Query: ' + query);
          console.error(error);
          reject(error);
        });
    });
  }

  /**
   * Fetch a single result item from query
   *
   * @param result
   * @returns {Array}
   */
  fetch(result): Array<any> {
    if(result.rows.length === 0) {
      return null;
    }
    return result.rows.item(0);
  }

  /**
   * Fetch in an array all the results from executed query
   *
   * @param result
   * @returns {Array}
   */
  fetchAll(result): Array<any> {
    let output = [];

    for (let i = 0; i < result.rows.length; i++) {
      output.push(result.rows.item(i));
    }
    return output;
  }

  /**
   * Insert a row on the given table
   * columns: [{name: 'name', value: 'value'}]
   *
   * @param table
   * @param columns
   * @returns {Promise}
   */

  insert(table, columns): Promise<any> {
    let columnsName = '';
    let columnsDefinition = '';
    let columnsValue = [];

    columns.forEach((column) => {
      if (columnsName.length > 0) {
        columnsName += ', ';
        columnsDefinition += ', ';
      }
      columnsName += column.name;
      columnsDefinition += '?';
      columnsValue.push(column.value);
    });

    return this.execute('INSERT INTO ' + table + ' (' + columnsName + ') values(' + columnsDefinition + ')', columnsValue)
      .then(function(result) {
        return result;
      });
  }

  /**
   * Select every row and column from some table
   *`
   * @param table
   * @param orderBy
   * @param where
   * @returns {Promise}
   */
  findAll(table, orderBy?, where?, limit?): Promise<any> {
    orderBy = (typeof orderBy !== 'undefined' && orderBy.length > 0) ? ' ORDER BY ' + orderBy : '';
    limit = (typeof limit !== 'undefined' && limit > 0) ? ' LIMIT ' + limit : '';
    let whereCondition = (typeof where !== 'undefined' && where.length > 0) ? ' WHERE ' : '';

    let parameterValue = [];

    //TODO Alterar para angular.isUndefined
    if (typeof where !== 'undefined') {
      where.forEach(column => {
        if (parameterValue.length > 0) {
          whereCondition += ' and ';
        }
        whereCondition += column.name + ' ' + column.operator + ' ?';
        parameterValue.push(column.value);
      });
    }

    return this.execute('SELECT * FROM ' + table + ' ' + whereCondition + ' ' + orderBy + ' ' + limit, parameterValue)
      .then(result => {
        return this.fetchAll(result);
      });
  }

  /**
   * Select a single row correspondent to given ID
   *
   * @param table
   * @param id
   * @returns {Promise}
   */
  findById(table, id): Promise<any> {
    return this.execute('SELECT * FROM ' + table + ' WHERE id = ?', [id])
      .then((result) => {
        return this.fetch(result);
      });
  }

  /**
   * Updates some row values
   * newValues: [{name: 'name', value: 'value'}]
   * paramters: [{name: 'name', value: 'value'}]
   *
   * @param table
   * @param newValues
   * @param parameters
   * @returns {Promise}
   */
  update(table, newValues, parameters): Promise<any> {
    let parameterValue = [];

    let newValuesDefinition = '';
    newValues.forEach((column) => {
      if (newValuesDefinition.length > 0) {
        newValuesDefinition += ', ';
      }
      newValuesDefinition += column.name + ' = ?';
      parameterValue.push(column.value);
    });

    let parameterDefinition = '';
    parameters.forEach((column) => {
      if (parameterDefinition.length > 0) {
        parameterDefinition += ' and ';
      }
      parameterDefinition += column.name + '= ?';
      parameterValue.push(column.value);
    });

    let whereCondition = '';
    if (parameterDefinition.length > 0) {
      whereCondition = ' WHERE ' + parameterDefinition;
    }

    return this.execute('UPDATE ' + table + ' SET ' + newValuesDefinition + whereCondition, parameterValue)
      .then(function(result) {
        return result;
      });
  }

  /**
   * Delete rows that match the given parameters
   * parameters: [{name: 'name', value: 'value'}]
   *
   * @param table
   * @param parameters
   * @returns {Promise}
   */
  delete(table, parameters): Promise<any> {
    let parameterDefinition = '';
    let parameterValue = [];


    //TODO Parameters é obrigatório, deve ser tratado.

    parameters.forEach(column => {
      if (parameterDefinition.length > 0) {
        parameterDefinition += ' and ';
      }
      parameterDefinition += column.name + '= ?';
      parameterValue.push(column.value);
    });

    return this.execute('DELETE FROM ' + table + ' WHERE ' + parameterDefinition, parameterValue)
      .then(result => {
        return result;
      });
  }

  /**
   * Delete a single row correspondent to the given ID
   *
   * @param table
   * @param id
   * @returns {Promise}
   */
  deleteById(table, id): Promise<any> {
    return this.execute('DELETE FROM ' + table + ' WHERE id = ?', [id])
      .then(result => {
        return result;
      });
  }

  /**
   * Delete every row on the given table
   *
   * @param table
   * @returns {Promise}
   */
  clearTable(table): Promise<any> {
    return this.execute('DELETE FROM ' + table)
      .then(result => {
        return result;
      });
  }
}