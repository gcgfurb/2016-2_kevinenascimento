import {App} from 'ionic-angular';

export class Database {

  databaseName: string;
  tables: Table[] = [];


  constructor(databaseName: string, tables: Table[]) {
    this.databaseName = databaseName;
    this.tables = tables;
  }

}
