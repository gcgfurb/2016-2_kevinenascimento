import {App} from 'ionic-angular';

export class DatabaseMigrations {

  /* EXAMPLE
   {
     version: 2, //should always be an integer value
     queries: [
     'ALTER TABLE product ADD COLUMN quantitySale integer;',
     'ALTER TABLE product ADD COLUMN totalQuantitySale integer;'
     ]
   }
   */
  public static get MIGRATIONS(): any {
    return [];
  }
}
