import {Directive, ElementRef, Input, OnInit, OnDestroy} from 'angular2/core';
import {Gesture} from 'ionic-angular/gestures/gesture';
declare var Hammer: any;

@Directive({
  selector: '[drag-with-reset]'
})

export class DragWithResetDirective implements OnInit, OnDestroy {
  @Input() posX;
  @Input() posY;

  private el: HTMLElement;
  private pressGesture: Gesture;
  private transform: any;
  private value: string[];
  public src: any;

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
    this.transform = {
      translate: {},
      scale: 1,
      angle: 0,
      rx: 0,
      ry: 0,
      rz: 0
    }
  }

  ngOnInit() {
    this.el.draggable = true;

    this.pressGesture = new Gesture(this.el, {
      recognizers: [
        [Hammer.Pan, { time: 500 }]
      ]
    });
    this.pressGesture.listen();

    this.pressGesture.on('panmove', e => {
      console.log('x: ' + e.deltaX);
      console.log('Y: ' + e.deltaY);
      this.transform.translate = {
        x: e.deltaX,
        y: e.deltaY
      };
      this.positionValid(e.deltaX, e.deltaY);
      this.updateElementTransform();
    });

    this.pressGesture.on('hammer.input', e => {
      if(e.isFinal) {
        console.log(e);
        if(!this.positionValid(e.deltaX, e.deltaY)) {
          this.resetElement();
        } else {
          this.transform.translate = {
            x: e.deltaX,
            y: e.deltaY
          };
          this.updateElementTransform();
        }
      }
    });
  }

  ngOnDestroy() {
    this.pressGesture.destroy();
  }

  positionValid(currentX, currentY) {
    //coordenadas do centro da circunferencia
    let circX = this.posX;
    let circY = this.posY;

    let dpc = Math.sqrt((Math.pow((currentX - circX), 2) + Math.pow(currentY - circY, 2)));
    let raio = Math.sqrt(5);

    console.log(dpc);
    console.log(raio);

    return dpc <= raio;
  }

  updateElementTransform() {
    let value: string[];
    value = [
      'translate3d(' + this.transform.translate.x + 'px, ' + this.transform.translate.y + 'px, 0)',
      'scale(' + this.transform.scale + ', ' + this.transform.scale + ')',
      'rotate3d(' + this.transform.rx + ',' + this.transform.ry + ',' + this.transform.rz + ',' + this.transform.angle + 'deg)'
    ];
    this.el.style.transform = value.join(" ");
  }

   resetElement() {
    this.transform = {
      translate: { x: 0, y: 0 },
      scale: 1,
      angle: 0,
      rx: 0,
      ry: 0,
      rz: 0
    };
    this.updateElementTransform();
  }
}