import {Directive, ElementRef, Input, OnInit, OnDestroy} from 'angular2/core';
import {Gesture} from 'ionic-angular/gestures/gesture';
declare var Hammer: any;

@Directive({
  selector: '[drag]'
})

export class DragDirective implements OnInit, OnDestroy {
  @Input() posX;
  @Input() posY;

  private el: HTMLElement;
  private pressGesture: Gesture;
  private transform: any;
  private value: string[];
  public src: any;

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
    this.transform = {
      translate: {},
      scale: 1,
      angle: 0,
      rx: 0,
      ry: 0,
      rz: 0
    }
  }

  ngOnInit() {
    this.el.draggable = true;

    this.pressGesture = new Gesture(this.el, {
      recognizers: [
        [Hammer.Pan, { time: 500 }]
      ]
    });
    this.pressGesture.listen();

    this.pressGesture.on('panmove', e => {
      this.transform.translate = {
        x: e.deltaX,
        y: e.deltaY
      };
      this.updateElementTransform();
    });

    this.pressGesture.on('hammer.input', e => {
      if(e.isFinal) {
        this.el.setAttribute("currentX", e.deltaX);
        this.el.setAttribute("currentY", e.deltaY);
      }
    });
  }

  ngOnDestroy() {
    this.pressGesture.destroy();
  }

  updateElementTransform() {
    let value: string[];
    value = [
      'translate3d(' + this.transform.translate.x + 'px, ' + this.transform.translate.y + 'px, 0)',
      'scale(' + this.transform.scale + ', ' + this.transform.scale + ')',
      'rotate3d(' + this.transform.rx + ',' + this.transform.ry + ',' + this.transform.rz + ',' + this.transform.angle + 'deg)'
    ];
    this.el.style.transform = value.join(" ");
  }
}