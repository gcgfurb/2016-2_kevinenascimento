/// <reference path='../../typings/tsd.d.ts' />

import mysql = require("mysql");

export class DatabaseService implements IConnectionConfig {

  connection: IConnection;

  constructor(user: string, password: string, database: string) {
    let options: IConnectionOptions;
    options.user = user;
    options.passwor = password;
    options.database = database;
    this.connection = mysql.createConnection(options);
  }
}
