/// <reference path='../typings/tsd.d.ts' />

import {WalkingService} from "./../service/walking.service";
import express = require('express');

let router = express.Router();
let walkingService = new WalkingService();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.route('/walking')
  .get(function(req, res) {
    res.send(walkingService.find(req.query));
  })
  .post(function(req, res) {
    console.log('post');
    //console.loig(req);
    let test = JSON.parse(req.body);
    console.log(test);
    //res.send(walkingService.save(req.query.walking));
    //walkingService.save(req.query.walking);
    res.send('Salvado com sucesso!');
  })
  .delete(function(req, res, next) {
    walkingService.delete();
  });

export = router;
